package com.sprinter.rxdemo;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import com.sprinter.rxdemo.content.common.context.AppContext;
import com.sprinter.rxdemo.content.common.context.AppContextFactoryImpl;

import timber.log.Timber;

public class App extends Application {

    private AppContext mAppContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @NonNull
    public AppContext getAppContext() {
        return mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        Timber.i("Timber init");

        mAppContext = new AppContextFactoryImpl().create(this);
    }

}
