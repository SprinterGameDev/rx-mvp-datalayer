package com.sprinter.rxdemo.ui.activity;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.sprinter.rxdemo.content.common.context.AppContext;
import com.trello.rxlifecycle.LifecycleTransformer;

/* package */ interface BaseActivityView {

    @NonNull
    @CheckResult
    <T> LifecycleTransformer<T> bindToLifecycle();

    @NonNull
    AppContext getAppContext();

}
