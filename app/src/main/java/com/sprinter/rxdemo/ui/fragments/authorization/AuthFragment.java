package com.sprinter.rxdemo.ui.fragments.authorization;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.ui.fragments.BaseFragment;
import com.sprinter.rxdemo.ui.fragments.dialogs.ProgressDialogFragment;
import com.sprinter.rxdemo.ui.fragments.profile.ProfileFragment;
import com.sprinter.rxdemo.utils.BindLayout;

import butterknife.BindView;
import butterknife.OnClick;

@BindLayout(R.layout.fragment_auth)
public class AuthFragment extends BaseFragment implements AuthView {

    public static final String FRAGMENT_TAG = "AuthFragment";

    @BindView(R.id.authorization_edit_login)
    EditText mEditNameView;
    @BindView(R.id.authorization_edit_password)
    EditText mEditPasswordView;
    @BindView(R.id.authorization_text_error)
    TextView mTextErrorView;

    private AuthFragmentPresenterImpl presenter;

    public static AuthFragment newInstance() {
        return new AuthFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new AuthFragmentPresenterImpl(this);
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.authorization_btn_signIn, R.id.authorization_btn_signUp})
    /* package */ void onClickSignIn(@NonNull final View view) {
        mTextErrorView.setVisibility(View.INVISIBLE);
        presenter.onClickAuthorization(mEditNameView.getText().toString(),
                mEditPasswordView.getText().toString(), view.getId() == R.id.authorization_btn_signUp);
    }

    @Override
    public void showError(@StringRes int errorResId) {
        mTextErrorView.setText(getString(errorResId));
        mTextErrorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressWait() {
        ProgressDialogFragment.showDialog(getFragmentManager(),
                getString(R.string.dialog_progress_auth_message));
    }

    @Override
    public void hideProgressWait() {
        ProgressDialogFragment.hideDialog(getFragmentManager());
    }

    @Override
    public void navigateToNext() {
        switchFragment(ProfileFragment.newInstance(), true, ProfileFragment.FRAGMENT_TAG);
    }

    @Override
    public void onDestroyView() {
        ProgressDialogFragment.hideDialog(getFragmentManager());
        presenter.onDestroyView();
        presenter = null;
        super.onDestroyView();
    }

}
