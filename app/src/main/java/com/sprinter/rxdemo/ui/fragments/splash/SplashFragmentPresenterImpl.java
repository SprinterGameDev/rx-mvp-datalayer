package com.sprinter.rxdemo.ui.fragments.splash;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sprinter.rxdemo.content.common.context.AppContext;
import com.sprinter.rxdemo.content.models.UserModel;

import rx.android.schedulers.AndroidSchedulers;

/* package */ class SplashFragmentPresenterImpl implements SplashFragmentPresenter {

    private static final long MIN_STEP_TIMER_MS = 100;
    private static final long MAX_STEP_TIMER_MS = 1500;

    private final long mStartSplashTime;
    @NonNull
    private SplashView mSplashView;
    @Nullable
    private CountDownTimer mTimer;

    /* package */ SplashFragmentPresenterImpl(@NonNull final SplashView splashView) {
        mSplashView = splashView;
        final AppContext appContext = mSplashView.getAppContext();
        mStartSplashTime = System.currentTimeMillis();

        appContext.getAccountManager().restoreSession();

        appContext.getAccountManager()
                .getUser()
                .distinctUntilChanged()
                .compose(splashView.bindToLifecycle())
                .onErrorReturn(this::onUserUpdateError)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onUserUpdate);
    }

    private UserModel onUserUpdateError(@NonNull final Throwable throwable) {
        return new UserModel();
    }

    private void onUserUpdate(@Nullable final UserModel userModel) {
        final long sysTimeDiff = (System.currentTimeMillis() - mStartSplashTime);
        final long diff = Math.max(0, MAX_STEP_TIMER_MS - sysTimeDiff);

        mTimer = new CountDownTimer(diff, MIN_STEP_TIMER_MS) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (mSplashView == null) {
                    return;
                }
                if (userModel != null && !TextUtils.isEmpty(userModel.getName()) && userModel.getId() != 0) {
                    mSplashView.showProfile();
                } else {
                    mSplashView.showAuthorization();
                }
            }
        }.start();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onDestroyView() {
        mSplashView = null;
        if (mTimer != null) {
            mTimer.cancel();
        }
        mTimer = null;
    }

}
