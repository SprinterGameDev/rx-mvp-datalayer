package com.sprinter.rxdemo.ui.fragments.profile;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sprinter.rxdemo.content.common.context.AppContext;
import com.sprinter.rxdemo.content.models.UserModel;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/* package */ class ProfileFragmentPresenterImpl implements ProfileFragmentPresenter {

    @NonNull
    private AppContext mAppContext;
    @NonNull
    private Subscription mUserSubscribe;
    @NonNull
    private ProfileView mProfileVew;
    @Nullable
    private UserModel mUserModel;

    /* package */ ProfileFragmentPresenterImpl(@NonNull final ProfileView profileView) {
        mProfileVew = profileView;

        mAppContext = profileView.getAppContext();
        final Observable<UserModel> userObservable = mAppContext.getAccountManager().getUser();
        mUserSubscribe = userObservable
                .distinctUntilChanged()
                .compose(profileView.bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onUserUpdate);
    }

    private void onUserUpdate(@NonNull final UserModel userModel) {
        mProfileVew.enableEditTextName(false);
        mProfileVew.visibleButtonChangeName(true);
        mProfileVew.visibleButtonApply(false);
        mUserModel = userModel;

        if (userModel.getId() == 0) {
            mProfileVew.onLogout();
        } else {
            mProfileVew.updateUserName(userModel.getName());
        }
    }

    @Override
    public void onClickApply(@NonNull final String name) {
        if (mUserModel != null && TextUtils.equals(mUserModel.getName(), name)) {
            onUserUpdate(mUserModel);
            return;
        }
        mProfileVew.enableEditTextName(false);
        mAppContext.getAccountManager().changeUserName(name);
    }

    @Override
    public void onClickChangeName() {
        mProfileVew.enableEditTextName(true);
        mProfileVew.visibleButtonChangeName(false);
        mProfileVew.visibleButtonApply(true);
    }

    @Override
    public void onClickLogout() {
        mProfileVew.showProgressLogout();
        mAppContext.getAccountManager().logout();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onDestroyView() {
        mUserSubscribe.unsubscribe();
        mUserSubscribe = null;
        mProfileVew = null;
        mAppContext = null;
    }

}
