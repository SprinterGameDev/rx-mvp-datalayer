package com.sprinter.rxdemo.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* package */ class MainActivityPresenterImpl implements MainActivityPresenter {

    @NonNull
    private MainView mMainView;

    /* package */ MainActivityPresenterImpl(@NonNull final MainView mainView) {
        mMainView = mainView;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mMainView.openSplashFragment();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onDestroy() {
        mMainView = null;
    }

}
