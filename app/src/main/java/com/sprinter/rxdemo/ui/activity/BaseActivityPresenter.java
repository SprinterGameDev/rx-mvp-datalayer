package com.sprinter.rxdemo.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

public interface BaseActivityPresenter {

    void onCreate(@Nullable final Bundle savedInstanceState);

    void onDestroy();

}
