package com.sprinter.rxdemo.ui.fragments.authorization;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.content.common.context.AppContext;
import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/* package */ class AuthFragmentPresenterImpl implements AuthFragmentPresenter {

    @NonNull
    private AppContext mAppContext;
    @NonNull
    private Subscription mUserSubscribe;
    @NonNull
    private AuthView mAuthView;

    /* package */ AuthFragmentPresenterImpl(@NonNull final AuthView authView) {
        this.mAuthView = authView;
        mAppContext = mAuthView.getAppContext();
        subscribe();
    }

    private void subscribe() {
        if (mUserSubscribe != null) {
            mUserSubscribe.unsubscribe();
        }
        mUserSubscribe = mAppContext.getAccountManager()
                .getUser()
                .distinctUntilChanged()
                .compose(mAuthView.bindToLifecycle())
                .onErrorReturn(this::onUserUpdateError)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onUserUpdate);
    }

    private UserModel onUserUpdateError(@NonNull final Throwable throwable) {
        return new UserModel();
    }

    private void onUserUpdate(@Nullable final UserModel userModel) {
        if (userModel != null && !TextUtils.isEmpty(userModel.getName()) && userModel.getId() != 0) {
            mAuthView.hideProgressWait();
            mAuthView.navigateToNext();
        } else {
            mAuthView.hideProgressWait();
            mAuthView.showError(R.string.auth_error_unknown);
        }
    }

    @Override
    public void onClickAuthorization(@NonNull final String name, @NonNull final String password,
                                     final boolean isNewUser) {
        if (validateFields(name, password)) {
            mAuthView.showProgressWait();
            subscribe();
            mAppContext.getAccountManager().authUser(new AuthModel(AuthModel.NO_ID, name, password),
                    isNewUser);
        }
    }

    private boolean validateFields(@Nullable final String name, @Nullable final String password) {
        if (TextUtils.isEmpty(name)) {
            mAuthView.showError(R.string.auth_error_empty_login);
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            mAuthView.showError(R.string.auth_error_empty_password);
            return false;
        }
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onDestroyView() {
        mUserSubscribe.unsubscribe();
        mUserSubscribe = null;
        mAuthView = null;
        mAppContext = null;
    }

}
