package com.sprinter.rxdemo.ui.fragments.profile;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.ui.fragments.BaseFragment;
import com.sprinter.rxdemo.ui.fragments.dialogs.ProgressDialogFragment;
import com.sprinter.rxdemo.ui.fragments.splash.SplashFragment;
import com.sprinter.rxdemo.utils.BindLayout;

import butterknife.BindView;
import butterknife.OnClick;

@BindLayout(R.layout.fragment_profile)
public class ProfileFragment extends BaseFragment implements ProfileView {

    public static final String FRAGMENT_TAG = "ProfileFragment";

    @BindView(R.id.profile_edit_name)
    EditText mEditNameView;
    @BindView(R.id.profile_btn_change_name)
    TextView mBtnChangeNameView;
    @BindView(R.id.profile_btn_apply_name)
    View mBtnApplyNameView;

    private ProfileFragmentPresenterImpl presenter;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ProfileFragmentPresenterImpl(this);
        mBtnChangeNameView.setPaintFlags(mEditNameView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void updateUserName(@NonNull final String name) {
        mEditNameView.setText(name);
    }

    @Override
    public void enableEditTextName(final boolean enabled) {
        mEditNameView.setEnabled(enabled);
    }

    @Override
    public void visibleButtonApply(final boolean visible) {
        mBtnApplyNameView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void visibleButtonChangeName(final boolean visible) {
        mBtnChangeNameView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.profile_btn_logout)
    /* package */ void onClickLogout() {
        presenter.onClickLogout();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.profile_btn_apply_name)
    /* package */ void onClickApply() {
        presenter.onClickApply(mEditNameView.getText().toString());
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.profile_btn_change_name)
    /* package */ void onClickChangeName() {
        presenter.onClickChangeName();
    }

    @Override
    public void onLogout() {
        ProgressDialogFragment.hideDialog(getFragmentManager());
        switchFragment(SplashFragment.newInstance(), true, SplashFragment.FRAGMENT_TAG);
    }

    @Override
    public void showProgressLogout() {
        ProgressDialogFragment.showDialog(getFragmentManager(),
                getString(R.string.dialog_progress_logout_message));
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        presenter = null;
        super.onDestroyView();
    }

}
