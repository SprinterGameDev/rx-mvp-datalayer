package com.sprinter.rxdemo.ui.fragments;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.sprinter.rxdemo.content.common.context.AppContext;
import com.trello.rxlifecycle.LifecycleTransformer;

public interface BaseFragmentView {

    @NonNull
    @CheckResult
    <T> LifecycleTransformer<T> bindToLifecycle();

    @NonNull
    AppContext getAppContext();

}
