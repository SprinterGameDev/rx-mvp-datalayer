package com.sprinter.rxdemo.ui.fragments.authorization;

import android.support.annotation.NonNull;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentPresenter;

/* package */ interface AuthFragmentPresenter extends BaseFragmentPresenter {

    void onClickAuthorization(@NonNull final String name, @NonNull final String password,
                              final boolean isNewUser);

}
