package com.sprinter.rxdemo.ui.activity.main;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentView;

/* package */ interface MainView extends BaseFragmentView {

    void openSplashFragment();

}
