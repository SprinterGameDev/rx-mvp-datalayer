package com.sprinter.rxdemo.ui.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.sprinter.rxdemo.ui.activity.BaseActivity;
import com.sprinter.rxdemo.utils.BindLayout;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/* package */ abstract class AbstractDialogFragment extends DialogFragment implements
        OnDialogButtonClickListener {

    protected boolean mIsViewBinded;
    private Unbinder mUnbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        BindLayout annotation = getInjectLayoutAnnotation();
        if (annotation != null) {
            return inflateAndInject(annotation.value(), inflater, container);
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    @Nullable
    private BindLayout getInjectLayoutAnnotation() {
        BindLayout annotation;
        Class typeToLookUp = getClass();
        while (true) {
            annotation = (BindLayout) typeToLookUp.getAnnotation(BindLayout.class);
            if (annotation != null) {
                break;
            }
            typeToLookUp = typeToLookUp.getSuperclass();
            if (typeToLookUp == null) {
                break;
            }
        }
        return annotation;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mIsViewBinded = true;
        super.onViewCreated(view, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        return dialog;
    }

    protected View inflateAndInject(int layoutId, LayoutInflater inflater, ViewGroup container) {
        final View view = inflater.inflate(layoutId, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void switchFragment(Fragment fragment, boolean clearBackStack, String tag) {
        final FragmentActivity activity = getActivity();
        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).switchFragment(fragment, clearBackStack, tag);
        }
    }

    public void onDialogButtonClick(final int requestCode, final int buttonId,
                                    @Nullable final Bundle baggage) {
        final Fragment fragment = this.getTargetFragment();
        final FragmentActivity activity = this.getActivity();

        if (fragment instanceof OnDialogButtonClickListener) {
            ((OnDialogButtonClickListener) fragment).onDialogButtonClick(requestCode, buttonId, baggage);
        } else if (activity instanceof OnDialogButtonClickListener) {
            ((OnDialogButtonClickListener) activity).onDialogButtonClick(requestCode, buttonId, baggage);
        }
    }

    @Override
    public void onDestroyView() {
        mIsViewBinded = false;
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.onDestroyView();
    }

}
