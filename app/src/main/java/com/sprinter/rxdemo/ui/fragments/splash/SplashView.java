package com.sprinter.rxdemo.ui.fragments.splash;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentView;

/* package */ interface SplashView extends BaseFragmentView {

    void showAuthorization();

    void showProfile();

}
