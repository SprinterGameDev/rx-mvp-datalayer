package com.sprinter.rxdemo.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sprinter.rxdemo.App;
import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.content.common.context.AppContext;
import com.sprinter.rxdemo.utils.BindLayout;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import butterknife.ButterKnife;
import timber.log.Timber;

public abstract class BaseActivity extends RxAppCompatActivity implements BaseActivityView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final BindLayout layout = getClass().getAnnotation(BindLayout.class);
        if (layout != null) {
            setContentView(layout.value());
            ButterKnife.bind(this, this);
        }
    }

    public <V extends Fragment> void switchFragment(@NonNull final V fragment,
                                                    final boolean clearBackStack,
                                                    @NonNull final String tag) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (clearBackStack) {
            try {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException e) {
                Timber.e(e.getMessage());
            }
        }

        try {
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(tag)
                    .replace(R.id.activity_main_container, fragment, tag)
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        final InputMethodManager inputMethodManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        final View view = this.getCurrentFocus() == null ? new View(this) : this.getCurrentFocus();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        if (backStackEntryCount < 2) {
            supportFinishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }

    @NonNull
    @Override
    public AppContext getAppContext() {
        return ((App) getApplication()).getAppContext();
    }

}
