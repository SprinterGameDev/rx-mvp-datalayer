package com.sprinter.rxdemo.ui.fragments.authorization;

import android.support.annotation.StringRes;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentView;

/* package */ interface AuthView extends BaseFragmentView {

    void showError(@StringRes final int errorResId);

    void showProgressWait();

    void hideProgressWait();

    void navigateToNext();

}
