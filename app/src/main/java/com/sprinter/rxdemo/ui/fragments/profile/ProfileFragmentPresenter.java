package com.sprinter.rxdemo.ui.fragments.profile;

import android.support.annotation.NonNull;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentPresenter;

/* package */ interface ProfileFragmentPresenter extends BaseFragmentPresenter {

    void onClickApply(@NonNull final String name);

    void onClickChangeName();

    void onClickLogout();

}
