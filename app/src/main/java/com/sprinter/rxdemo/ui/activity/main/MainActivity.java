package com.sprinter.rxdemo.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.ui.activity.BaseActivity;
import com.sprinter.rxdemo.ui.fragments.splash.SplashFragment;
import com.sprinter.rxdemo.utils.BindLayout;

@BindLayout(R.layout.activity_main)
public class MainActivity extends BaseActivity implements MainView {

    @NonNull
    private MainActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MainActivityPresenterImpl(this);
        mPresenter.onCreate(savedInstanceState);
    }

    @Override
    public void openSplashFragment() {
        switchFragment(SplashFragment.newInstance(), true, SplashFragment.FRAGMENT_TAG);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onDestroy() {
        mPresenter.onDestroy();
        mPresenter = null;
        super.onDestroy();
    }

}
