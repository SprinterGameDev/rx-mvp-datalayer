package com.sprinter.rxdemo.ui.fragments.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.sprinter.rxdemo.R;

import lombok.NonNull;
import timber.log.Timber;

public class ProgressDialogFragment extends AbstractDialogFragment {

    private static final String FRAGMENT_TAG = "ProgressDialogFragment";
    private static final String BUNDLE_ARG_TITLE = "BUNDLE_ARG_TITLE";
    private static final String BUNDLE_ARG_MESSAGE = "BUNDLE_ARG_MESSAGE";

    public static ProgressDialogFragment showDialog(@NonNull final FragmentManager manager) {
        return showDialog(manager, null, null);
    }

    public static ProgressDialogFragment showDialog(@NonNull final FragmentManager manager,
                                                    @NonNull final String message) {
        return showDialog(manager, null, message);
    }

    public static ProgressDialogFragment showDialog(@NonNull final FragmentManager manager,
                                                    @Nullable final String title,
                                                    @Nullable final String message) {
        hideDialog(manager);

        final Bundle args = new Bundle();
        args.putString(BUNDLE_ARG_TITLE, title);
        args.putString(BUNDLE_ARG_MESSAGE, message);

        final ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setArguments(args);
        try {
            fragment.show(manager, FRAGMENT_TAG);
        } catch (Exception e) {
            Timber.e("%s: %s", FRAGMENT_TAG, e.getMessage());
        }

        return fragment;
    }

    public static void hideDialog(@NonNull final FragmentManager manager) {
        final ProgressDialogFragment fragment = (ProgressDialogFragment) manager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment != null) {
            try {
                fragment.dismissAllowingStateLoss();
            } catch (Exception e) {
                Timber.w("%s: %s", FRAGMENT_TAG, e.getMessage());
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @android.support.annotation.NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getString(R.string.app_name);
        String message = getString(R.string.dialog_progress_default_message);
        final Bundle args = getArguments();
        if (args != null) {
            title = args.getString(BUNDLE_ARG_TITLE, title);
            message = args.getString(BUNDLE_ARG_MESSAGE, message);
        }

        final ProgressDialog dialog = new ProgressDialog(getActivity(), getTheme());
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return dialog;
    }

}
