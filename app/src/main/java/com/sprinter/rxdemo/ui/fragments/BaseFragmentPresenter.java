package com.sprinter.rxdemo.ui.fragments;

public interface BaseFragmentPresenter {

    void onDestroyView();

}
