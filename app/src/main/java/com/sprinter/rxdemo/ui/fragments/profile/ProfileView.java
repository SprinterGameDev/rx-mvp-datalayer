package com.sprinter.rxdemo.ui.fragments.profile;

import android.support.annotation.NonNull;

import com.sprinter.rxdemo.ui.fragments.BaseFragmentView;

/* package */ interface ProfileView extends BaseFragmentView {

    void enableEditTextName(final boolean enabled);

    void visibleButtonApply(final boolean visible);

    void visibleButtonChangeName(final boolean visible);

    void updateUserName(@NonNull final String name);

    void onLogout();

    void showProgressLogout();

}
