package com.sprinter.rxdemo.ui.fragments.splash;

import android.os.Bundle;
import android.view.View;

import com.sprinter.rxdemo.R;
import com.sprinter.rxdemo.ui.fragments.BaseFragment;
import com.sprinter.rxdemo.ui.fragments.authorization.AuthFragment;
import com.sprinter.rxdemo.ui.fragments.profile.ProfileFragment;
import com.sprinter.rxdemo.utils.BindLayout;

@BindLayout(R.layout.fragment_splash)
public class SplashFragment extends BaseFragment implements SplashView {

    public static final String FRAGMENT_TAG = "SplashFragment";

    private SplashFragmentPresenterImpl mPresenter;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter = new SplashFragmentPresenterImpl(this);
    }

    @Override
    public void showAuthorization() {
        switchFragment(AuthFragment.newInstance(), true, AuthFragment.FRAGMENT_TAG);
    }

    @Override
    public void showProfile() {
        switchFragment(ProfileFragment.newInstance(), true, ProfileFragment.FRAGMENT_TAG);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDestroyView();
        mPresenter = null;
        super.onDestroyView();
    }

}
