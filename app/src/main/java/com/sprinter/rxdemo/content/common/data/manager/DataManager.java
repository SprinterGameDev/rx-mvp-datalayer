package com.sprinter.rxdemo.content.common.data.manager;

import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import lombok.NonNull;
import rx.Observable;

public interface DataManager {

    Observable<UserModel> userRegistration(@NonNull final AuthModel authModel);

    Observable<UserModel> userAuthorization(@NonNull final AuthModel authModel);

    Observable<UserModel> updateUserData(@NonNull final UserModel userModel);

    Observable<UserModel> restoreSession();

    Observable<UserModel> logout();

}
