package com.sprinter.rxdemo.content.common.context;

import android.content.Context;
import android.support.annotation.NonNull;

/* package */ interface AppContextFactory {

    @NonNull
    AppContext create(@NonNull final Context context);

}
