package com.sprinter.rxdemo.content.common.data.database.dao;

import android.support.annotation.NonNull;

import com.j256.ormlite.support.ConnectionSource;
import com.sprinter.rxdemo.content.models.UserModel;

import java.sql.SQLException;

public final class UserDao extends BaseDao<UserModel, Integer> {

    public UserDao(@NonNull final ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UserModel.class);
    }

}
