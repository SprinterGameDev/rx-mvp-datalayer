package com.sprinter.rxdemo.content.common.data.manager.user;

import android.support.annotation.WorkerThread;

import com.sprinter.rxdemo.content.common.data.database.DataBaseManager;
import com.sprinter.rxdemo.content.common.data.preferense.PreferenceManager;
import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import java.util.concurrent.Callable;

import lombok.NonNull;
import rx.Observable;

public final class UserManagerImpl implements UserManager {

    private static final long FAKE_REQUEST_DELAY_MS = 1500;

    @NonNull
    private final DataBaseManager mDataBaseManager;
    @NonNull
    private final PreferenceManager mPreferenceManager;

    public UserManagerImpl(@NonNull final DataBaseManager dataBaseManager,
                           @NonNull final PreferenceManager preferenceManager) {
        this.mDataBaseManager = dataBaseManager;
        this.mPreferenceManager = preferenceManager;
    }

    @Override
    @WorkerThread
    @SuppressWarnings("ConstantConditions")
    public Observable<UserModel> userRegistration(@NonNull final AuthModel authModel) {
        return Observable.combineLatest(
                fakeDelayRequest(),
                mDataBaseManager.getAuthDao().createData(authModel),
                (aVoid, model) ->
                        mDataBaseManager.getUserDao().createData(new UserModel(
                                model.getId(), model.getLogin())).toBlocking().first())
                .doOnCompleted(() -> mPreferenceManager.putLastAuthModel(authModel));
    }

    @Override
    @WorkerThread
    @SuppressWarnings("ConstantConditions")
    public Observable<UserModel> userAuthorization(@NonNull final AuthModel authModel) {
        return Observable.combineLatest(
                fakeDelayRequest(),
                mDataBaseManager.getAuthDao().getAuthData(authModel),
                (aVoid, model) -> mDataBaseManager.getUserDao().queryForId(model.getId()))
                .doOnCompleted(() -> mPreferenceManager.putLastAuthModel(authModel))
                .doOnError(throwable -> mPreferenceManager.removeAuthModel());
    }

    @Override
    @WorkerThread
    public Observable<UserModel> updateUserData(@NonNull final UserModel userModel) {
        return mDataBaseManager.getUserDao().updateData(userModel);
    }

    @Override
    @WorkerThread
    public Observable<UserModel> restoreSession() {
        return userAuthorization(mPreferenceManager.getLastAuthModel());
    }

    @Override
    @WorkerThread
    public Observable<UserModel> logout() {
        return Observable.fromCallable(() -> {
            mPreferenceManager.removeAuthModel();
            fakeDelayRequest().toBlocking().first();
            return new UserModel();
        });
    }

    @WorkerThread
    private Observable<Void> fakeDelayRequest() {
        return Observable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Thread.sleep(FAKE_REQUEST_DELAY_MS);
                return null;
            }
        });
    }

}
