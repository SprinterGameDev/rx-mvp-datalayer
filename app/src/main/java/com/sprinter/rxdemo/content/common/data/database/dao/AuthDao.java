package com.sprinter.rxdemo.content.common.data.database.dao;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.j256.ormlite.support.ConnectionSource;
import com.sprinter.rxdemo.content.models.AuthModel;

import java.sql.SQLException;

import rx.Observable;

public final class AuthDao extends BaseDao<AuthModel, Integer> {

    public AuthDao(@NonNull final ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AuthModel.class);
    }

    @Nullable
    public Observable<AuthModel> getAuthData(@NonNull final AuthModel authModel) {
        return Observable.fromCallable(() ->
                AuthDao.super.queryBuilder()
                        .where()
                        .eq(AuthModel.COLUMN_LOGIN, authModel.getLogin())
                        .and()
                        .eq(AuthModel.COLUMN_PASSWORD, authModel.getPassword())
                        .queryForFirst());
    }

}
