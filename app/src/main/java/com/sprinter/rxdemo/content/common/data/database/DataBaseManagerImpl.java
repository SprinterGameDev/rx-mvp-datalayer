package com.sprinter.rxdemo.content.common.data.database;

import android.content.Context;
import android.support.annotation.Nullable;

import com.sprinter.rxdemo.content.common.data.database.dao.AuthDao;
import com.sprinter.rxdemo.content.common.data.database.dao.UserDao;

import java.sql.SQLException;

import lombok.NonNull;

public class DataBaseManagerImpl implements DataBaseManager {

    @NonNull
    private static volatile DataBaseManagerImpl instance;
    @Nullable
    private static DataBaseHelper mDatabaseHelper;
    @Nullable
    private AuthDao mAuthDao;
    @Nullable
    private UserDao mUserDao;

    public static DataBaseManagerImpl getInstance() {
        DataBaseManagerImpl localInstance = instance;
        if (localInstance == null) {
            synchronized (DataBaseManagerImpl.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataBaseManagerImpl();
                }
            }
        }
        return localInstance;
    }

    private DataBaseManagerImpl() {
        if (mDatabaseHelper == null) {
            throw new IllegalStateException("You must first call the 'DataBaseManagerImpl::initialize'");
        }
    }

    public static void initialize(@NonNull final Context context) {
        mDatabaseHelper = new DataBaseHelper(context);
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public AuthDao getAuthDao() {
        if (mAuthDao == null) {
            try {
                mAuthDao = new AuthDao(mDatabaseHelper.getConnectionSource());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return mAuthDao;
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public UserDao getUserDao() {
        if (mUserDao == null) {
            try {
                mUserDao = new UserDao(mDatabaseHelper.getConnectionSource());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return mUserDao;
    }

    @SuppressWarnings({"ConstantConditions", "unused"})
    public void dropAllSavedData() {
        mDatabaseHelper.dropAllSavedData();
    }

}
