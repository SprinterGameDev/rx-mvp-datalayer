package com.sprinter.rxdemo.content.common.data.manager.user;

import android.support.annotation.WorkerThread;

import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import lombok.NonNull;
import rx.Observable;

public interface UserManager {

    @NonNull
    @WorkerThread
    Observable<UserModel> userRegistration(@NonNull final AuthModel authModel);

    @NonNull
    @WorkerThread
    Observable<UserModel> userAuthorization(@NonNull final AuthModel authModel);

    @NonNull
    @WorkerThread
    Observable<UserModel> updateUserData(@NonNull final UserModel userModel);

    @NonNull
    @WorkerThread
    Observable<UserModel> restoreSession();

    @NonNull
    @WorkerThread
    Observable<UserModel> logout();

}
