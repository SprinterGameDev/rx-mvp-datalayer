package com.sprinter.rxdemo.content.models;

import android.support.annotation.IntRange;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@Data
@EqualsAndHashCode(callSuper = false)
@DatabaseTable(tableName = "auth")
public class AuthModel extends AbstractModel {

    public static final int NO_ID = 0;

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_PASSWORD = "password";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    int id;

    @DatabaseField(columnName = COLUMN_LOGIN, unique = true)
    String login;

    @DatabaseField(columnName = COLUMN_PASSWORD)
    String password;

    public AuthModel() {
        id = NO_ID;
        login = "";
        password = "";
    }

    public AuthModel(@IntRange(from = 0) final int id, @NonNull final String login,
                     @NonNull final String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

}
