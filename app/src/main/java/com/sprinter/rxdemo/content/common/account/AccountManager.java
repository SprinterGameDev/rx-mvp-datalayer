package com.sprinter.rxdemo.content.common.account;

import android.support.annotation.NonNull;

import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import rx.Observable;

public interface AccountManager {

    void authUser(@NonNull final AuthModel authModel, final boolean isNewUser);

    void restoreSession();

    void logout();

    void changeUserName(@NonNull final String name);

    Observable<UserModel> getUser();

}
