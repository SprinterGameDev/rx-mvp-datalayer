package com.sprinter.rxdemo.content.common.account;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.sprinter.rxdemo.content.common.data.manager.DataManager;
import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public final class AppAccountManagerImpl implements AccountManager {

    @NonNull
    private BehaviorSubject<UserModel> mAuthDataSubject;
    @NonNull
    private final DataManager mDataManger;
    @Nullable
    private UserModel mActiveUser;

    public AppAccountManagerImpl(@NonNull final DataManager dataManager) {
        mAuthDataSubject = BehaviorSubject.create();
        mDataManger = dataManager;
    }

    @Override
    public void authUser(@NonNull final AuthModel authModel, final boolean isNewUser) {
        final Observable<UserModel> observable = isNewUser ?
                mDataManger.userRegistration(authModel) :
                mDataManger.userAuthorization(authModel);

        observable.subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .onErrorReturn(this::onError)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onActivateUser);
    }

    @Override
    public void restoreSession() {
        mActiveUser = null;
        resetObservable();
        mDataManger.restoreSession()
                .subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .onErrorReturn(this::onError)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onActivateUser);
    }

    private UserModel onError(@NonNull final Throwable throwable) {
        mAuthDataSubject.onError(throwable);
        return null;
    }

    private void onActivateUser(@Nullable final UserModel activeUser) {
        mActiveUser = activeUser;
        mAuthDataSubject.onNext(activeUser);
        resetObservable();
    }

    @Override
    public void logout() {
        mActiveUser = null;
        mDataManger.logout()
                .subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .onErrorReturn(this::onError)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(this::resetObservable)
                .subscribe(mAuthDataSubject::onNext);
    }


    @Override
    public void changeUserName(@NonNull final String name) {
        if (mActiveUser != null) {
            final UserModel model = UserModel.clone(mActiveUser);
            model.setName(name);
            mDataManger.updateUserData(model)
                    .subscribeOn(Schedulers.io())
                    .distinctUntilChanged()
                    .onErrorReturn(this::onError)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onActivateUser);
        }
    }

    @Nullable
    @Override
    public Observable<UserModel> getUser() {
        return mAuthDataSubject;
    }

    private void resetObservable() {
        if (mActiveUser == null) {
            mAuthDataSubject.onCompleted();
            mAuthDataSubject = BehaviorSubject.create();
        }
    }

}
