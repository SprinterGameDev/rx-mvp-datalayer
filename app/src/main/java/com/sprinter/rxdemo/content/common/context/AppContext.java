package com.sprinter.rxdemo.content.common.context;

import android.support.annotation.NonNull;

import com.sprinter.rxdemo.content.common.account.AccountManager;
import com.sprinter.rxdemo.content.common.data.manager.DataManager;

public final class AppContext {

    @NonNull
    private final AccountManager mAccountManager;
    @NonNull
    private final DataManager mDataManger;

    /* package */ AppContext(@NonNull final AccountManager mAccountManager,
                             @NonNull final DataManager dataManager) {
        this.mAccountManager = mAccountManager;
        this.mDataManger = dataManager;
    }

    @NonNull
    public AccountManager getAccountManager() {
        return mAccountManager;
    }

    @NonNull
    public DataManager getDataManger() {
        return mDataManger;
    }

}
