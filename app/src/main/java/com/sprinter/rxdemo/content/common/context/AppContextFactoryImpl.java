package com.sprinter.rxdemo.content.common.context;

import android.content.Context;
import android.support.annotation.NonNull;

import com.sprinter.rxdemo.content.common.account.AppAccountManagerImpl;
import com.sprinter.rxdemo.content.common.data.database.DataBaseManagerImpl;
import com.sprinter.rxdemo.content.common.data.manager.DataManager;
import com.sprinter.rxdemo.content.common.data.manager.DataManagerFacadeImpl;
import com.sprinter.rxdemo.content.common.data.manager.user.UserManagerImpl;
import com.sprinter.rxdemo.content.common.data.preferense.PreferenceManagerImpl;

public final class AppContextFactoryImpl implements AppContextFactory {

    @NonNull
    @Override
    public AppContext create(@NonNull final Context context) {
        DataBaseManagerImpl.initialize(context);
        PreferenceManagerImpl.initialize(context);

        final DataManager dataManager = new DataManagerFacadeImpl(new UserManagerImpl(
                DataBaseManagerImpl.getInstance(), PreferenceManagerImpl.getInstance()));
        return new AppContext(new AppAccountManagerImpl(dataManager), dataManager);
    }

}
