package com.sprinter.rxdemo.content.common.data.database;

import com.sprinter.rxdemo.content.common.data.database.dao.AuthDao;
import com.sprinter.rxdemo.content.common.data.database.dao.UserDao;

import lombok.NonNull;

public interface DataBaseManager {

    @NonNull
    AuthDao getAuthDao();

    @NonNull
    UserDao getUserDao();

}
