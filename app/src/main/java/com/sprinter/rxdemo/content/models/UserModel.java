package com.sprinter.rxdemo.content.models;

import android.support.annotation.IntRange;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@Data
@EqualsAndHashCode(callSuper = false)
@DatabaseTable(tableName = "users")
public class UserModel extends AbstractModel {

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";

    @DatabaseField(id = true, columnName = COLUMN_ID)
    final int id;

    @DatabaseField(columnName = COLUMN_NAME)
    String name;

    @NonNull
    public static UserModel clone(@NonNull final UserModel model) {
        return new UserModel(model.getId(), model.getName());
    }

    public UserModel() {
        id = AuthModel.NO_ID;
        name = "";
    }

    public UserModel(@IntRange(from = 0) final int id, @NonNull final String name) {
        this.id = id;
        this.name = name;
    }

}
