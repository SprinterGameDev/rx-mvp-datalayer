package com.sprinter.rxdemo.content.common.data.preferense;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.sprinter.rxdemo.content.models.AuthModel;

import lombok.NonNull;
import timber.log.Timber;

public final class PreferenceManagerImpl implements PreferenceManager {
    private static final String KEY_AUTH_MODEL = "KEY_AUTH_MODEL";

    @NonNull
    private static volatile PreferenceManagerImpl sInstance;
    @Nullable
    private static SharedPreferences mPreferences;

    @NonNull
    private final Gson mGson;

    private PreferenceManagerImpl() {
        mGson = new Gson();
    }

    @NonNull
    public static PreferenceManagerImpl getInstance() {
        PreferenceManagerImpl localInstance = sInstance;
        if (localInstance == null) {
            synchronized (PreferenceManagerImpl.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new PreferenceManagerImpl();
                }
            }
        }
        return localInstance;
    }

    public static void initialize(@NonNull final Context context) {
        if (mPreferences == null) {
            mPreferences = android.preference.PreferenceManager.getDefaultSharedPreferences(
                    context.getApplicationContext());
        }
    }

    @NonNull
    private SharedPreferences getPreferences() {
        if (mPreferences == null) {
            throw new IllegalStateException("You must first call the 'PreferenceManagerImpl::initialize'");
        }
        return mPreferences;
    }

    @NonNull
    @Override
    public AuthModel getLastAuthModel() {
        final String json = getPreferences().getString(KEY_AUTH_MODEL, "");
        try {
            final AuthModel model = mGson.fromJson(json, AuthModel.class);
            return model == null ? new AuthModel(AuthModel.NO_ID, "", "") : model;
        } catch (Exception e) {
            Timber.w(e.getMessage());
            return new AuthModel(AuthModel.NO_ID, "", "");
        }
    }

    @Override
    public void putLastAuthModel(@NonNull final AuthModel model) {
        put(KEY_AUTH_MODEL, new Gson().toJson(model, AuthModel.class));
    }

    @Override
    public void removeAuthModel() {
        remove(KEY_AUTH_MODEL);
    }

    @SuppressWarnings("unused")
    private void put(@NonNull final String key, final int val) {
        getPreferences().edit().putInt(key, val).apply();
    }

    private void put(@NonNull final String key, @NonNull final String val) {
        getPreferences().edit().putString(key, val).apply();
    }

    @SuppressWarnings("unused")
    private void put(@NonNull final String key, final boolean val) {
        getPreferences().edit().putBoolean(key, val).apply();
    }

    private void remove(String key) {
        getPreferences().edit().remove(key).apply();
    }

}
