package com.sprinter.rxdemo.content.common.data.database.dao;

import android.support.annotation.Nullable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rx.Observable;

/* package */ class BaseDao<T, ID> extends BaseDaoImpl<T, ID> {

    /* package */ BaseDao(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public int addAll(final Collection<T> dataList) throws SQLException {
        return callBatchTasks(() -> {
            Integer count = 0;
            for (T data : dataList) {
                createIfNotExists(data);
            }
            return count;
        });
    }

    public int addOrUpdateAll(final Collection<T> dataList) throws SQLException {
        return callBatchTasks(() -> {
            Integer count = 0;
            for (T data : dataList) {
                createOrUpdate(data);
            }
            return count;
        });
    }

    @Override
    public List<T> queryForAll() {
        final List<T> list;
        try {
            list = super.queryForAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list == null ? new ArrayList<>() : list;
    }


    @Nullable
    public Observable<T> createData(final T data) {
        return Observable.fromCallable(() -> {
            final int count = BaseDao.super.create(data);
            return count > 0 ? BaseDao.this.queryForSameId(data) : null;
        });
    }

    @Override
    public int update(T data) {
        final int result;
        try {
            result = super.update(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public Observable<T> updateData(T data) {
        return Observable.fromCallable(() -> {
            final int count = BaseDao.super.update(data);
            return count > 0 ? BaseDao.this.queryForSameId(data) : null;
        });
    }

    @Override
    public int delete(T data) {
        final int result;
        try {
            result = super.delete(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public long countOf() {
        final long result;
        try {
            result = super.countOf();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public int deleteById(ID id) {
        final int result;
        try {
            result = super.deleteById(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;

    }

    @Override
    public T queryForId(ID id) {
        T result = null;
        try {
            if (id != null) {
                result = super.queryForId(id);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public void removeAll() {
        try {
            this.delete(deleteBuilder().prepare());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
