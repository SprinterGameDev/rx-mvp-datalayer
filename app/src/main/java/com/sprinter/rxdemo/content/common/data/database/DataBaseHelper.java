package com.sprinter.rxdemo.content.common.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sprinter.rxdemo.content.models.AbstractModel;
import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import lombok.NonNull;

/* package */ class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "rx.demo.db";
    private static final int DATABASE_VERSION = 1;

    private static final List<Class<? extends AbstractModel>> tablesClasses =
            new ArrayList<Class<? extends AbstractModel>>() {
                {
                    add(AuthModel.class);
                    add(UserModel.class);
                }
            };

    /* package */ void dropAllSavedData() {
        for (Class<? extends AbstractModel> tablesClass : tablesClasses) {
            try {
                TableUtils.clearTable(connectionSource, tablesClass);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* package */ DataBaseHelper(@NonNull final Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        for (Class<? extends AbstractModel> tablesClass : tablesClasses) {
            try {
                TableUtils.createTable(connectionSource, tablesClass);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        for (Class<? extends AbstractModel> tablesClass : tablesClasses) {
            try {
                TableUtils.dropTable(connectionSource, tablesClass, true);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        onCreate(sqLiteDatabase, connectionSource);
    }

}
