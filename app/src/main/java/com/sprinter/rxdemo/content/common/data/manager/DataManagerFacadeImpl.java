package com.sprinter.rxdemo.content.common.data.manager;

import android.support.annotation.WorkerThread;

import com.sprinter.rxdemo.content.common.data.manager.user.UserManager;
import com.sprinter.rxdemo.content.models.AuthModel;
import com.sprinter.rxdemo.content.models.UserModel;

import lombok.NonNull;
import rx.Observable;

public final class DataManagerFacadeImpl implements DataManager {

    private final UserManager mUserManager;

    public DataManagerFacadeImpl(@NonNull final UserManager userManager) {
        this.mUserManager = userManager;
    }

    @Override
    @WorkerThread
    @SuppressWarnings("ConstantConditions")
    public Observable<UserModel> userRegistration(@NonNull final AuthModel authModel) {
        return mUserManager.userRegistration(authModel);
    }

    @Override
    @WorkerThread
    @SuppressWarnings("ConstantConditions")
    public Observable<UserModel> userAuthorization(@NonNull final AuthModel authModel) {
        return mUserManager.userAuthorization(authModel);
    }

    @Override
    @WorkerThread
    public Observable<UserModel> updateUserData(@NonNull final UserModel userModel) {
        return mUserManager.updateUserData(userModel);
    }

    @Override
    @WorkerThread
    public Observable<UserModel> restoreSession() {
        return mUserManager.restoreSession();
    }

    @Override
    @WorkerThread
    public Observable<UserModel> logout() {
        return mUserManager.logout();
    }

}
