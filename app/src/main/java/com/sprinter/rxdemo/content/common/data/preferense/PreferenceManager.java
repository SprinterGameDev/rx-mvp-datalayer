package com.sprinter.rxdemo.content.common.data.preferense;

import com.sprinter.rxdemo.content.models.AuthModel;

import lombok.NonNull;

public interface PreferenceManager {

    AuthModel getLastAuthModel();

    void putLastAuthModel(@NonNull final AuthModel model);

    void removeAuthModel();

}
